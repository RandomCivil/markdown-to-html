module gitlab.com/RandomCivil/markdown-to-html

go 1.20

require github.com/gomarkdown/markdown v0.0.0-20220627144906-e9a81102ebeb

require (
	github.com/alecthomas/chroma v0.10.0
	gitlab.com/RandomCivil/common v0.0.0-20210108050723-5ab78e84b182
)

require github.com/dlclark/regexp2 v1.4.0 // indirect
