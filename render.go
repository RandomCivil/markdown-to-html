package markdowntohtml

import (
	"bytes"
	"encoding/base64"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"

	"github.com/alecthomas/chroma/quick"
	md "github.com/gomarkdown/markdown"

	"github.com/gomarkdown/markdown/ast"
	"github.com/gomarkdown/markdown/html"
)

type render struct {
	needMermaid bool
	srcDir      string
}

func NewRender(srcDir string) *render {
	return &render{
		srcDir: srcDir,
	}
}

func (r *render) Rendering(w io.Writer, node ast.Node, entering bool) (ast.WalkStatus, bool) {
	if cb, ok := node.(*ast.CodeBlock); ok {
		if bytes.Equal(cb.Info, []byte("mermaid")) {
			io.WriteString(w, fmt.Sprintf("<div class=\"mermaid\">%s</div>", string(cb.Literal)))
			r.needMermaid = true
		} else { // code
			quick.Highlight(w, string(cb.Literal), string(cb.Info), "html", "github")
		}
		return ast.GoToNext, true
	}
	if cb, ok := node.(*ast.Image); ok && entering {
		imagePath := string(cb.Destination)
		fmt.Printf("image: %s\n", imagePath)
		if strings.HasPrefix(imagePath, "http://") || strings.HasPrefix(imagePath, "https://") {
			return ast.GoToNext, false
		}

		absPath, err := filepath.Abs(r.srcDir + "/" + imagePath)
		if err != nil {
			fmt.Printf("convert to absolute path error:%+v,imagePath:%s\n", err, imagePath)
			return ast.GoToNext, false
		}
		fmt.Printf("absPath: %s\n", absPath)

		if _, err := os.Stat(absPath); os.IsNotExist(err) {
			fmt.Printf("path does not exist: %s\n", absPath)
			return ast.GoToNext, false
		}

		imgData, err := ioutil.ReadFile(absPath)
		fmt.Printf("imgData: %d err:%+v\n", len(imgData), err)
		if err != nil {
			fmt.Printf("read image file error:%+v,imagePath:%s\n", err, absPath)
			return ast.GoToNext, false
		}

		base64Image := base64.StdEncoding.EncodeToString(imgData)
		io.WriteString(w, fmt.Sprintf("<img src=\"data:image/png;base64,%s\"/>", base64Image))
		return ast.SkipChildren, true
	}
	return ast.GoToNext, false
}

func (r *render) HandleEntryFunc(v interface{}) interface{} {
	// render code block
	rr := html.NewRenderer(html.RendererOptions{
		RenderNodeHook: r.Rendering,
	})
	// render markdown
	return string(md.ToHTML([]byte(v.(string)), nil, rr))
}

func (r *render) EchoNeedMermaid() interface{} {
	return r.needMermaid
}
