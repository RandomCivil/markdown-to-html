package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"
	"text/template"

	_ "embed"

	"gitlab.com/RandomCivil/common"
	markdowntohtml "gitlab.com/RandomCivil/markdown-to-html"
)

const (
	MARKDOWN_PARAM       = "markdown"
	MARKDOWN_HANDLE_FUNC = "handleEntryFunc"
	ECHO_NEED_MERMAID    = "echoNeedMermaid"
)

var (
	src     = flag.String("src", "", "data file path or directory path")
	dst     = flag.String("dst", "", "destination directory path")
	version = flag.Bool("version", false, "Show current version")
	//go:embed template.html
	tplData string
)

func checkFile(path string) (bool, error) {
	info, err := os.Stat(path)
	if err != nil {
		return false, err
	}
	return !info.IsDir(), nil
}

func findMarkdownFiles(root string) ([]string, error) {
	var markdownFiles []string
	err := filepath.Walk(root, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if !info.IsDir() && (strings.HasSuffix(info.Name(), ".md") || strings.HasSuffix(info.Name(), ".markdown")) {
			markdownFiles = append(markdownFiles, path)
		}
		return nil
	})

	return markdownFiles, err
}

func renderOne(src, dst string) {
	srcData, err := os.ReadFile(src)
	if err != nil {
		log.Println("read data file error", err)
		return
	}

	srcDir := filepath.Dir(src)
	fmt.Println("srcDir", srcDir)

	srcFileName := filepath.Base(src)
	f, err := os.Create(dst + strings.TrimSuffix(srcFileName, filepath.Ext(srcFileName)) + ".html")
	if err != nil {
		log.Println("create dst file error", err)
		return
	}
	defer f.Close()

	data := map[string]interface{}{
		MARKDOWN_PARAM: string(srcData),
	}
	rd := markdowntohtml.NewRender(srcDir)

	tpl, err := template.New("").Funcs(template.FuncMap{MARKDOWN_HANDLE_FUNC: rd.HandleEntryFunc, ECHO_NEED_MERMAID: rd.EchoNeedMermaid}).Parse(tplData)
	if err != nil {
		log.Println("template parse file error", err)
		return
	}

	if err = tpl.Execute(f, data); err != nil {
		log.Println("rendor error", err)
	}
}

func main() {
	flag.Parse()
	common.Verbose()
	if *version {
		return
	}

	isFile, err := checkFile(*src)
	if err != nil {
		log.Println("src file not exist")
		return
	}

	fmt.Println("isFile", isFile)
	if isFile {
		renderOne(*src, *dst)
		return
	}

	mdFiles, err := findMarkdownFiles(*src)
	if err != nil {
		log.Println("find markdown files error", err)
		return
	}
	fmt.Println("mdFiles", mdFiles)

	for _, file := range mdFiles {
		renderOne(file, *dst)
	}
}
