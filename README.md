# markdown-to-html

```bash
Usage of markdown-to-html:
  -dst string
        destination directory path
  -src string
        data file path or directory path
  -version
        Show current version

# render all markdown files in directory
markdown-to-html -src test/ -dst /mnt/d/dst/

# render one markdown file
markdown-to-html -src test/redis.md -dst /mnt/d/dst/
```

## feature

- markdown to html
- mermaid support
- code highlight support
- render all markdown files in directory